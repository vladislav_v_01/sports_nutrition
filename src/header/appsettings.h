#ifndef APPSETTINGS_H
#define APPSETTINGS_H

#include <QAudioFormat>
#include <QObject>
#include <QSettings>
#include <qqml.h>

#include "models.h"
#include "property.h"

class AppSettings : public QObject
{
    Q_OBJECT
    INI_PROPERTY(int, age, 0);
    INI_PROPERTY(int, height, 0);
    INI_PROPERTY(int, weight, 0);

public:
    enum HumanGender
    {
        men,
        women,
        unknown
    };
    Q_ENUM(HumanGender);

private:
    INI_PROPERTY(HumanGender, gender, HumanGender::unknown);

public:
    Q_INVOKABLE AppSettings(QObject* parent = nullptr);
    Q_INVOKABLE ~AppSettings();

private:
    void init();
    // данные о пользователе
private:
    //    std::shared_ptr<QSettings> m_dataUser;
};

#endif // APPSETTINGS_H
