#ifndef PROPERTY_H
#define PROPERTY_H

#include <QObject>
#include <QSettings>
#include <QSharedPointer>

class GlobalSettings;
class GlobalSettings : public QSettings
{
    Q_OBJECT

public:
    static QSharedPointer<GlobalSettings> instance();

private:
    GlobalSettings(GlobalSettings::Format format,
                   GlobalSettings::Scope  sope,
                   const QString&         organization,
                   const QString&         application = QString());
    static QSharedPointer<GlobalSettings> m_instance;
};

#define PROPERTY(type, name)                                                   \
    PROPERTY_DECL(type, name)                                                  \
    PROPERTY_SET(type, name)                                                   \
    PROPERTY_GET(type, name)                                                   \
    PROPERTY_SIGNAL(type, name)

#define PROPERTY_DEFAULT(type, name, default)                                  \
    PROPERTY_DECL_DEFAULT(type, name, default)                                 \
    PROPERTY_SET(type, name)                                                   \
    PROPERTY_GET(type, name)                                                   \
    PROPERTY_SIGNAL(type, name)

#define PROPERTY_DEBUG(type, name)                                             \
    PROPERTY_DECL(type, name)                                                  \
    PROPERTY_SET_DEBUG(type, name)                                             \
    PROPERTY_GET_DEBUG(type, name)                                             \
    PROPERTY_SIGNAL(type, name)

#define PROPERTY_CUSTOM_SET(type, name)                                        \
    PROPERTY_DECL(type, name)                                                  \
    PROPERTY_GET(type, name)                                                   \
    PROPERTY_SIGNAL(type, name)                                                \
public Q_SLOTS:                                                                \
    virtual void set_##name(type a);

#define PROPERTY_DECL(type, name)                                              \
    QML_DECLARATION(type, name)                                                \
protected:                                                                     \
    type m_##name;

#define PROPERTY_DECL_DEFAULT(type, name, default)                             \
    QML_DECLARATION(type, name)                                                \
protected:                                                                     \
    type m_##name = default;

#define QML_DECLARATION(type, name)                                            \
protected:                                                                     \
    Q_PROPERTY(                                                                \
        type name READ get_##name WRITE set_##name NOTIFY name##Changed FINAL)

#define PROPERTY_SET(type, name)                                               \
public Q_SLOTS:                                                                \
    virtual void set_##name(type a)                                            \
    {                                                                          \
        if (m_##name == a)                                                     \
        {                                                                      \
            return;                                                            \
        }                                                                      \
        m_##name = a;                                                          \
        emit name##Changed();                                                  \
    }

#define PROPERTY_SET_DEBUG(type, name)                                         \
public Q_SLOTS:                                                                \
    virtual void set_##name(type a)                                            \
    {                                                                          \
        DEBUG_INFO("PROPERTY: SET" << #name << a);                             \
        m_##name = a;                                                          \
        emit name##Changed();                                                  \
    }

#define PROPERTY_GET(type, name)                                               \
public:                                                                        \
    Q_INVOKABLE virtual type& get_##name() { return m_##name; }                \
    Q_INVOKABLE virtual type  get_##name() const { return m_##name; }

#define PROPERTY_GET_DEBUG(type, name)                                         \
public:                                                                        \
    virtual type get_##name() const                                            \
    {                                                                          \
        qDebug() << "PROPERTY: GET" << #name << m_##name;                      \
        return m_##name;                                                       \
    }

#define PROPERTY_SIGNAL(type, name)                                            \
Q_SIGNALS:                                                                     \
    void name##Changed()

#define INI_PROPERTY_SET(type, name)                                           \
public Q_SLOTS:                                                                \
    virtual void set_##name(const type& a)                                     \
    {                                                                          \
        auto old = get_##name();                                               \
        if (old == a)                                                          \
        {                                                                      \
            return;                                                            \
        }                                                                      \
        auto n = QString(this->staticMetaObject.className()) + "/" + #name;    \
        GlobalSettings::instance()->setValue(n, a);                            \
        GlobalSettings::instance()->sync();                                    \
        emit name##Changed();                                                  \
    }

#define INI_PROPERTY_GET(type, name, defaultValue)                             \
public:                                                                        \
    virtual type get_##name() const                                            \
    {                                                                          \
        return static_cast<type>(                                              \
            GlobalSettings::instance()                                         \
                ->value(QString(this->staticMetaObject.className()) + "/" +    \
                            #name,                                             \
                        defaultValue)                                          \
                .value<type>());                                               \
    }

#define INI_PROPERTY_GET_CAST(type, name, defaultValue)                        \
public:                                                                        \
    virtual type get_##name() const                                            \
    {                                                                          \
        return GlobalSettings::instance()->value(                              \
            QString(this->staticMetaObject.className()) + "/" + #name,         \
            defaultValue);                                                     \
    }

#define INI_PROPERTY_GET_VAR(name)                                             \
public:                                                                        \
    virtual QVariant get_##name()                                              \
    {                                                                          \
        return GlobalSettings::instance()->value(                              \
            QString(this->staticMetaObject.className()) + "/" + #name);        \
    }

#define INI_PROPERTY(type, name, defaultValue)                                 \
    INI_PROPERTY_SET(type, name)                                               \
    INI_PROPERTY_GET(type, name, defaultValue)                                 \
    QML_DECLARATION(type, name)                                                \
    PROPERTY_SIGNAL(type, name)

#define INI_PROPERTY_CAST(type, name, defaultValue)                            \
    INI_PROPERTY_SET(type, name)                                               \
    INI_PROPERTY_GET_CAST(type, name, defaultValue)                            \
    QML_DECLARATION(type, name)                                                \
    PROPERTY_SIGNAL(type, name)

#define INI_PROPERTY_VAR(name)                                                 \
    INI_PROPERTY_SET(QVariant, name)                                           \
    INI_PROPERTY_GET_VAR(name)                                                 \
    QML_DECLARATION(QVariant, name)                                            \
    RPROPERTY_SIGNAL(QVariant, name)

#define INI_PROPERTY_DEBUG(name)                                               \
    INI_PROPERTY_SET(name)                                                     \
    INI_PROPERTY_GET(name)                                                     \
    QML_DECLARATION(type, name)                                                \
    PROPERTY_SIGNAL(type, name)

#endif // PROPERTY_H
