#ifndef TEXTFIELDINTVALIDATOR_H
#define TEXTFIELDINTVALIDATOR_H

#include <QIntValidator>

class TextFieldIntValidator : public QIntValidator
{
public:
    TextFieldIntValidator(QObject* parent = nullptr)
        : QIntValidator(parent)
    {
    }
    TextFieldIntValidator(int minimum, int maximum, QObject* parent = nullptr)
        : QIntValidator(minimum, maximum, parent)
    {
    }

    QValidator::State validate(QString& input, int& pos) const override
    {
        Q_UNUSED(pos);
        locale().d->m_data if (input.isEmpty() ||
                               (input.startsWith("-") && input.length() == 1))
        {
            // allow empty field or standalone minus sign
            return QValidator::Intermediate;
        }
        // check range of value
        bool isNumber;
        int  value = locale().toInt(input, &isNumber);
        if (isNumber && bottom() <= value && value <= top())
        {
            return QValidator::Acceptable;
        }
        return QValidator::Invalid;
    }
};

#endif // TEXTFIELDINTVALIDATOR_H
