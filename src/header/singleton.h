#ifndef SINGLETON_H
#define SINGLETON_H

#include <memory>
#include <mutex>

#define SINGLETON(type) Singleton<type>::getInstance().get()

template <class T>
class Singleton
{
public:
    static std::shared_ptr<T> getInstance()
    {
        std::lock_guard<std::mutex> lock(m_mutex);

        if (m_instance == nullptr)
        {
            m_instance = std::make_shared<T>();
        }
        return m_instance;
    }

    static void clean()
    {
        std::lock_guard<std::mutex> lock(m_mutex);

        m_instance.clear();
    }

    using FactorySharedPointer = std::shared_ptr<T>;

private:
    static FactorySharedPointer m_instance;
    static std::mutex           m_mutex;
};

template <class T>
typename Singleton<T>::FactorySharedPointer Singleton<T>::m_instance;

template <class T>
typename std::mutex Singleton<T>::m_mutex;

#endif // SINGLETON_H
