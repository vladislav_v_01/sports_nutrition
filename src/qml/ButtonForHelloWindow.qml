import QtQuick 2.15
import QtQuick.Controls 2.15

Button {
    id: button

//    implicitWidth: parent.width / 2 - parent.width / 10;
//    implicitHeight: 150

    property color backgroundColor: "lightgreen"
    onBackgroundColorChanged:{
        backgroundRectangle.color = backgroundColor}
    property color borderColor: "white"
    property int borderWidth: 0
    property int borderRadius: 100
    property int textPointSize: 20
    property color textColor: "white"

    signal clicked;
    signal pressed;
    signal released;

    contentItem: Text{
        id: buttonText

        text: button.text
        font.pointSize: textPointSize
        color: textColor
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    background: Rectangle{
        id: backgroundRectangle

//        implicitWidth: button.implicitWidth
//        implicitHeight: button.implicitHeight
        color: backgroundColor
        border.color: borderColor
        border.width: borderWidth
        radius: borderRadius
    }

    MouseArea{
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true

        onClicked: button.clicked();
        onPressed: button.pressed();
        onReleased: button.released();
    }
}

