import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

//import "."

//ScrollablePage{
Item{
    id: root;
    objectName: "helloWindow";

    GridLayout{
        anchors.fill: parent;
        columns: 1;
        rows: 2;

        RowLayout{
            id: rowLayout
            Layout.row: 0;
            Layout.column: 0;
            Layout.topMargin: 50;
            Layout.alignment: Qt.AlignTop;

            ButtonForHelloWindow{
                id: exitButton;
//                Layout.fillWidth: true;
                backgroundColor: "white";
                Layout.margins: 50;
                text: qsTr("\uf057");
                textColor: "limegreen";
                font.pointSize: 30;
                font.bold: true;
                onClicked: {
                    backgroundColor = "lightgreen";
                    close();
                }
            }

            Label{
            Layout.margins: 20;
            text:  qsTr("Your physical indicators.");
            font.pointSize: 20;
            color: "limegreen";
            }
        }

        ColumnLayout{
            id: columnLayout;
            Layout.row: 1;
            Layout.column: 0;

            Label{
                id: helloText;
                Layout.fillWidth: true;
                Layout.alignment: Qt.AlignVCenter;
                horizontalAlignment: Qt.AlignHCenter;
//                topPadding: parent.height / 4;
                text:  qsTr("Hello!");
                font.pointSize: 50;
                font.bold: true;
            }

            Label{
                Layout.bottomMargin: 50;
                Layout.topMargin: 50;
                Layout.fillWidth: true;
                Layout.alignment: Qt.AlignHCenter;
                horizontalAlignment: Qt.AlignHCenter;
                wrapMode: Label.Wrap;
                elide: Text.ElideMiddle;
                text:  qsTr("Let's start with physical indicators.");
                font.pointSize: 20;
            }

            RowLayout{
                Layout.fillWidth: true;
                Layout.margins: {
                    left: 20;
                    right: 20;
                }
                spacing: 20;

                ButtonForHelloWindow{
                    id: menButton;
                    Layout.preferredWidth: parent.width / 2;
                    implicitHeight: 150
                    text: qsTr("MEN");
                    onClicked: {
                        menButton.backgroundColor = "limegreen"
                        womenButton.backgroundColor = "lightgreen"
                        columnLayout.Layout.alignment = Qt.AlignTop;
//                        columnLayout.anchors.top = rowLayout.bottom
                        helloText.visible = false
                        weightTextField.visible = true;
                        heightTextField.visible = true;
                        ageTextField.visible = true;
                        AppSettings.gender = 0;
                    }

                    onPressed: {
                        menButton.backgroundColor = "limegreen";
                    }

                    onReleased: {
                        menButton.backgroundColor = "lightgreen";
                    }
                }

                ButtonForHelloWindow{
                    id: womenButton;
                    Layout.fillWidth: true;
                    implicitHeight: 150
                    text: qsTr("WOMEN");
                    onClicked: {
                        womenButton.backgroundColor = "limegreen"
                        menButton.backgroundColor = "lightgreen"
                        columnLayout.Layout.alignment = Qt.AlignTop;
//                        columnLayout.anchors.top = rowLayout.bottom
                        helloText.visible = false
                        weightTextField.visible = true;
                        heightTextField.visible = true;
                        ageTextField.visible = true;
                        AppSettings.gender = 1;
                    }

                    onPressed: {
                        womenButton.backgroundColor = "limegreen";
                    }

                    onReleased: {
                        womenButton.backgroundColor = "lightgreen";
                    }
                }
            }


            FloatingTextField {
                id: ageTextField

                Layout.fillWidth: true
                Layout.margins: 50;

                property bool lastInputState: true

                inputMethodHints: Qt.ImhDigitsOnly
                validator: IntValidator{
                    bottom: 14;
                    top: 100;
                }

                selectByMouse: true

                placeholderText: qsTr("Your age:")
                text: "";
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                font.pointSize: 20;

                property bool thereIsAgeRangeErrorLabel: false;

                onAcceptableInputChanged: {
                    if(ageTextField.visible){
                        if(!acceptableInput && ageTextField.text < 14){
                            if(!thereIsAgeRangeErrorLabel){
                                ageTextField.color = "lightgray";
                                ageBottomRangeErrorLabel.visible = true;
                                thereIsAgeRangeErrorLabel = true;
                            }
                        } else if(!acceptableInput && ageTextField.text > 100){
                            if(!thereIsAgeRangeErrorLabel){
                                ageTextField.color = "lightgray";
                                ageTopRangeErrorLabel.visible = true;
                                thereIsAgeRangeErrorLabel = true;
                            }
                        }else if(acceptableInput){
                            if(thereIsAgeRangeErrorLabel){
                                ageBottomRangeErrorLabel.visible = false;
                                ageTopRangeErrorLabel.visible = false;
                                thereIsAgeRangeErrorLabel = false;
                                ageTextField.color = "black";
                            }
                            if(weightTextField.acceptableInput && heightTextField.acceptableInput){
                                futherButton.visible = true;
                            }
                            AppSettings.age = parseInt(ageTextField.text, 10);
                            focus = false;
                        }
                    }
                }

                onActiveFocusChanged: {
                    if(ageTextField.visible){
                        if(!activeFocus && !acceptableInput && ageTextField.text < 14){
                            if(!thereIsAgeRangeErrorLabel){
                            ageTextField.color = "lightgray";
                            ageBottomRangeErrorLabel.visible = true;
                                thereIsAgeRangeErrorLabel = true;
                            }
                        }else if(!activeFocus && !acceptableInput && ageTextField.text > 100){
                            if(!thereIsAgeRangeErrorLabel){
                            ageTextField.color = "lightgray";
                            ageTopRangeErrorLabel.visible = true;
                                thereIsAgeRangeErrorLabel = true;
                            }
                        }
                    }
                }
                visible: false;
            }

            ErrorRangeLabel{
                id: ageBottomRangeErrorLabel

                Layout.leftMargin: 50;
                Layout.rightMargin: 50;
                Layout.fillWidth: true;

                text: qsTr("You must be at least 14 years old to use this application");

                visible: false;
            }

            ErrorRangeLabel{
                id: ageTopRangeErrorLabel

                Layout.leftMargin: 50;
                Layout.rightMargin: 50;
                Layout.fillWidth: true;

                text: qsTr("How OLD are you? Enter age no more than 100 years old");

                visible: false;
            }

            FloatingTextField {
                id: heightTextField

                Layout.fillWidth: true
                Layout.margins: 50;

                inputMethodHints: Qt.ImhDigitsOnly
                validator: IntValidator {
                    bottom: 50
                    top: 300
                }

                selectByMouse: true
                //enabled: root.enabledControls

                placeholderText: qsTr("Your height:")
                text: "";
                font.pointSize: 20;

                property bool thereIsHeightRangeErrorLabel: false;

                onAcceptableInputChanged: {
                    if(heightTextField.visible){
                        if(!acceptableInput && heightTextField.text < 50){
                            if(!thereIsHeightRangeErrorLabel){
                            heightTextField.color = "lightgray";
                            heightBottomRangeErrorLabel.visible = true
                            thereIsHeightRangeErrorLabel = true;
                            }
                        }else if(!acceptableInput && heightTextField.text > 300){
                            if(!thereIsHeightRangeErrorLabel){
                            heightTextField.color = "lightgray";
                            heightTopRangeErrorLabel.visible = true
                            thereIsHeightRangeErrorLabel = true;
                            }
                        }else if(acceptableInput){
                            if(thereIsHeightRangeErrorLabel){
                                heightBottomRangeErrorLabel.visible = false;
                                heightTopRangeErrorLabel.visible = false;
                                thereIsHeightRangeErrorLabel = false;
                                heightTextField.color = "black";
                            }
                            if(weightTextField.acceptableInput && ageTextField.acceptableInput){
                                futherButton.visible = true;
                            }
                            AppSettings.height = parseInt(heightTextField.text, 10);
                            focus = false;
                        }
                    }
                }

                onActiveFocusChanged: {
                    if(heightTextField.visible){
                        if(!activeFocus && !acceptableInput && heightTextField.text < 50){
                            if(!thereIsHeightRangeErrorLabel){
                            heightTextField.color = "lightgray";
                            heightBottomRangeErrorLabel.visible = true;
                            thereIsHeightRangeErrorLabel = true;
                            }
                        }else if(!activeFocus && !acceptableInput && heightTextField.text > 300){
                            if(!thereIsHeightRangeErrorLabel){
                            heightTextField.color = "lightgray";
                            heightTopRangeErrorLabel.visible = true;
                            thereIsHeightRangeErrorLabel = true;
                            }
                        }
                    }
                }

                visible: false;
            }

            ErrorRangeLabel{
                id: heightBottomRangeErrorLabel

                Layout.leftMargin: 50;
                Layout.rightMargin: 50;
                Layout.fillWidth: true;

                text: qsTr("Your height must be at least 50 cm");

                visible: false;
            }

            ErrorRangeLabel{
                id: heightTopRangeErrorLabel

                Layout.leftMargin: 50;
                Layout.rightMargin: 50;
                Layout.fillWidth: true;

                text: qsTr("This is GROWTH! Your height must not exceed 300 cm");

                visible: false;
            }

            FloatingTextField {
                id: weightTextField

                Layout.fillWidth: true
                Layout.margins: 50

                inputMethodHints: Qt.ImhDigitsOnly
                validator: IntValidator {
                    bottom: 30
                    top: 300
                }

                selectByMouse: true

                placeholderText: qsTr("Your growth:")
                text: "";
                font.pointSize: 20;

                property bool thereIsWeightRangeErrorLabel: false;

                onAcceptableInputChanged: {
                    if(weightTextField.visible){
                        if(!acceptableInput && weightTextField.text < 30){
                            if(!thereIsWeightRangeErrorLabel){
                            weightTextField.color = "lightgray";
                            weightBottomRangeErrorLabel.visible = true;
                            thereIsWeightRangeErrorLabel = true;
                            }
                        }else if(!acceptableInput && weightTextField.text > 300){
                            if(!thereIsWeightRangeErrorLabel){
                            weightTextField.color = "lightgray";
                            weightTopRangeErrorLabel.visible = true;
                            thereIsWeightRangeErrorLabel = true;
                            }
                        }else if(acceptableInput){
                            if(thereIsWeightRangeErrorLabel){
                                weightBottomRangeErrorLabel.visible = false;
                                weightTopRangeErrorLabel.visible = false;
                                thereIsWeightRangeErrorLabel = false;
                                weightTextField.color = "black";
                            }
                            if(heightTextField.acceptableInput && ageTextField.acceptableInput){
                                futherButton.visible = true;
                            }

                            AppSettings.weight = parseInt(weightTextField.text, 10);
                            focus = false;
                        }
                    }
                }

                onActiveFocusChanged: {
                    if(weightTextField.visible){
                        if(!activeFocus && !acceptableInput && weightTextField.text < 30){
                            if(!thereIsWeightRangeErrorLabel){
                            weightTextField.color = "lightgray";
                            weightBottomRangeErrorLabel.visible = false;
                            thereIsWeightRangeErrorLabel = true;
                            }
                        }else if(!activeFocus && !acceptableInput && weightTextField.text > 300){
                            if(!thereIsWeightRangeErrorLabel){
                            weightTextField.color = "lightgray";
                            weightTopRangeErrorLabel.visible = false;
                            thereIsWeightRangeErrorLabel = true;
                            }
                        }
                    }
                }

                visible: false;
            }

            ErrorRangeLabel{
                id: weightBottomRangeErrorLabel

                Layout.leftMargin: 50;
                Layout.rightMargin: 50;
                Layout.fillWidth: true;

                text: qsTr("Please make sure your weight is at least 30 kg ");

                visible: false;
            }

            ErrorRangeLabel{
                id: weightTopRangeErrorLabel

                Layout.leftMargin: 50;
                Layout.rightMargin: 50;
                Layout.fillWidth: true;

                text: qsTr("Is that correct? Please make sure your weight is less than 300 kg");

                visible: false;
            }

            ButtonForHelloWindow{
                id: futherButton;
                Layout.preferredWidth: parent.width / 2;
                Layout.alignment: Qt.AlignHCenter;
                implicitHeight: 150;
                text: qsTr("FUTHER");
                visible: false;

                onClicked: {
                    futherButton.backgroundColor = "limegreen";
                    stackView.pop();
                    stackView.push("MainWindow.qml");

                }

                onPressed: {
                    futherButton.backgroundColor = "limegreen";
                }

                onReleased: {
                    futherButton.backgroundColor = "lightgreen";
                }
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:1040;width:760}
}
##^##*/
