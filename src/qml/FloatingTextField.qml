import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.3
import QtQuick.Templates 2.5 as T

//import Fluid.Controls 1.1 as FluidControls

T.TextField {
    id: control

    implicitWidth: Math.max((background ? background.implicitWidth : 0),
                            placeholderText ?
                                placeholder.implicitWidth +
                                leftPadding + rightPadding : 0) ||
                   (contentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(contentHeight + topPadding + bottomPadding,
                             (background ? background.implicitHeight : 0),
                             placeholder.implicitHeight + topPadding + bottomPadding)

    topPadding: contentHeight
    bottomPadding: 8 * 2

    selectByMouse: true

//    color: enabled ? "limegreen" : "lightgreen"

    states: [
        State {
            name: "focused"
            when: control.activeFocus
            PropertyChanges {
                target: placeholder

                color: enabled ? "limegreen" : "lightgreen"
//                    font.pointSize: control.font.pointSize - 1
                anchors.topMargin: 0
            }
            PropertyChanges {
                target: bottomLine
                color: "limegreen";
            }
        },
        State {
            name: "notFocusedNotEmpty"
            when: !control.activeFocus && control.text != ""
            PropertyChanges {
                target: placeholder

//                    font.pointSize: control.font.pointSize - 1
                anchors.topMargin: 0
                color: enabled ? "limegreen" : "lightgreen"
            }
            PropertyChanges {
                target: bottomLine
                color: "limegreen";
            }
        },
        State {
            name: "notFocusedEmpty"
            when: !control.activeFocus && control.text == ""
            PropertyChanges {
                target: placeholder

                color:  "lightgreen";
                anchors.topMargin: offset * 2
//                    font.pointSize: control.font.pointSize + 2
            }
            PropertyChanges {
                target: bottomLine
                color: "lightgreen";
            }
        }
    ]

    transitions: [
        Transition {
            NumberAnimation {
                properties: "anchors.topMargin,font.pointSize"
                duration: 100
                easing.type: Easing.InOutQuad
            }
            ColorAnimation {
                duration: 100
            }
        }
    ]


    Label {
        id: placeholder

        anchors.top: parent.top
        x: control.leftPadding

        font: control.font
        text: control.placeholderText
        elide: Label.AlignRight
        width: control.width - (control.leftPadding + control.rightPadding)

        readonly property int offset: control.font.pointSize
    }

    background: Rectangle {
        id: bottomLine

        y: control.height - height - control.bottomPadding + 8
        width: control.width
        height: control.activeFocus || control.hovered ? 3 : 1
        color: "lightgreen";
    }
}
