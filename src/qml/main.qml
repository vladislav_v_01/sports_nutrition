import QtQuick 2.15
import QtQuick.Controls 2.15

import "."

ApplicationWindow{
    id: window;
    width: 720;
    height: 1040;
    visible: true;
    title: qsTr("Sport nutrition");

    FontLoader {
        id: awesome
        source: "qrc:/fonts/Font Awesome 5 Pro-Light-300.otf"
    }

    Shortcut{
        sequences: ["Esc", "Ctrl+q"];
        onActivated: {
            close();
        }
        context: Qt.ApplicationShortcut;
    }

    StackView{
        id: stackView;
        anchors.fill: parent;

        initialItem: helloWindow;

        Component{
            id: helloWindow;

            HelloWindow{}

        }
    }
//    Component.onCompleted: stackView.push("helloWindow.qml")
}
