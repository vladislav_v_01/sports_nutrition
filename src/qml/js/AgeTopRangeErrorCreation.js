var ageTopRangeErrorLabel;
var component;

function createAgeTopRangeErrorLabel() {
    component = Qt.createComponent("AgeTopRangeError.qml");
    ageTopRangeErrorLabel = component.createObject(columnLayout);

    if(ageTopRangeErrorLabel == null){
        console.log("Error creating AgeTopRangeErrorLabel.qml!");
    }
}
