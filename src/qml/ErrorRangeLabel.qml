import QtQuick 2.15
import QtQuick.Controls 2.15

Label{
    id: root

    topPadding: -49;

    visible: false;

    wrapMode: Text.Wrap;
    elide: Text.ElideMiddle;
    font.pointSize: 14;
    color: "red";
}
