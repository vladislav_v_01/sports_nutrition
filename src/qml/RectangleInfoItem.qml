import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import "controls"

Rectangle{
    id: backgroundRectangle;

    property string backgroundColor: "lightgray"
    property string borderColor: "black"
    property string imageSource: ""
    property string mainText: ""
    property string iconAwesome: "\uf055"
    property string iconAwesomeColor: "black"
    property string dividingLineColor: "black"
    property string additionalText: "Number of calories..."
    property int borderWidth: 1

//    anchors.horizontalCenter: parent.horizontalCenter;
//    anchors.verticalCenter: parent.verticalCenter;

    implicitWidth: parent.width - parent.width / 5;
    implicitHeight: column.height;

    color: backgroundColor;

    border.color: borderColor;
    border.width: borderWidth;

    z: 1;

    Column{
        id: column;

        spacing: 1;

        width: parent.width - parent.width / 100;

        anchors.horizontalCenter: parent.horizontalCenter;

        RowLayout{

            width: parent.width;

            Image {
                id: leftImage;

                Layout.topMargin: parent.height / 50;

                mipmap: true;

                sourceSize.height: mainTextLabel.height * 2;

                source: imageSource;
            }

            Label {
                id: mainTextLabel;

//                Layout.alignment: Qt.AlignLeft;
                horizontalAlignment: Qt.AlignLeft;


                text: qsTr(mainText);
                font.bold: true;
                font.pointSize: 22;
            }

            ClickableFontAwesomeIcon {
                Layout.preferredHeight: parent.singleItemHeight * 2;
                Layout.alignment: Qt.AlignRight;

                icon: iconAwesome;
                color: iconAwesomeColor;
                fontSize:30;
            }
        }

        Rectangle {
            id: dividingLine;

            width: parent.width;
            height: 1;

            color: dividingLineColor;
        }

        Label {
            id: additionalTextLabel;

            width: parent.width;

            wrapMode: Label.Wrap;

            horizontalAlignment: Qt.AlignHCenter;

            text: qsTr(additionalText);
        }
    }
}

