import QtQuick 2.15
import QtQuick.Controls 2.15

Label{
    id: ageBoxTopRangeError;

    text: qsTr("You must be at least 14 years old to use this application.");
    font.pointSize: 20;
}
