import QtQuick 2.12
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.12

//import FontAwesome 1.0

Item {
    id: root

    implicitWidth: size
    implicitHeight: size

    property int size: container.size + 4

    property string text: ""
    property alias containsMouse: mouseArea.containsMouse
    property alias icon: textItem.text
    property alias fontSize: textItem.font.pointSize
    property alias isMousePressed: mouseArea.pressed
    property alias color: textItem.color

    signal clicked()

    Item {
        id: container

        implicitWidth: size
        implicitHeight: size

        anchors.centerIn: parent
        opacity: root.isMousePressed ? 0.7 : 1

        property int calcHeight: columnLayout.implicitHeight
        property int size: Math.max(columnLayout.implicitHeight, columnLayout.implicitWidth)

        ColumnLayout {
            id: columnLayout
            anchors.centerIn: parent
            spacing: 0

            Item {
                implicitWidth: textItem.implicitWidth
                implicitHeight: textItem.implicitHeight

                Label {
                    id: textItem

                    font.pointSize: 18

                    anchors.centerIn: parent

                }
            }
        }
    }

    MouseArea {
        id: mouseArea

        anchors.fill: root
        hoverEnabled: true
        onClicked: root.clicked()

        z: 100

        cursorShape: Qt.PointingHandCursor
    }

    ToolTip.visible: ToolTip.text != "" && mouseArea.containsMouse
}
