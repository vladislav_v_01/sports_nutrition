import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

//import FontAwesome 1.0

import "controls"
//import "RectangleInfoItem.qml"

/*ScrollablePage*/ApplicationWindow {
    id: page;
    minimumHeight: 520
    minimumWidth: 360
    visible:  true;

    FontLoader {
        id: awesome
        source: "qrc:/fonts/Font Awesome 5 Pro-Light-300.otf"
    }

    ColumnLayout{
        id: mainLayout;

        anchors.fill: parent;

        RectangleInfoItem{
            id: breakfastItem;

            Layout.alignment: Qt.AlignHCenter;

            backgroundColor: "#FAFAFA";
            borderColor: "black"
            imageSource: "qrc:/img/breakfast.png";
            mainText: "Breakfast"
        }

        RectangleInfoItem{
            id: secondBreakfastItem;

            Layout.alignment: Qt.AlignHCenter;

            backgroundColor: "#FAFAFA";
            borderColor: "black"
            imageSource: "qrc:/img/second_breakfast.png";
            mainText: "Tiffin"
        }

        RectangleInfoItem{
            id: lunchItem;

            Layout.alignment: Qt.AlignHCenter;

            backgroundColor: "#FAFAFA";
            borderColor: "black"
            imageSource: "qrc:/img/lunch.png";
            mainText: "Lunch"
        }

        RectangleInfoItem{
            id: afterLunchItem;

            Layout.alignment: Qt.AlignHCenter;

            backgroundColor: "#FAFAFA";
            borderColor: "black"
            imageSource: "qrc:/img/after_lunch.png";
            mainText: "After lunch"
        }

        RectangleInfoItem{
            id: dinnerItem;

            Layout.alignment: Qt.AlignHCenter;

            backgroundColor: "#FAFAFA";
            borderColor: "black"
            imageSource: "qrc:/img/dinner.png";
            mainText: "Dinner"
        }
    }
}



/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
