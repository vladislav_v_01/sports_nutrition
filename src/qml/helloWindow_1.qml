import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

ApplicationWindow{
    id: mainWindow

    minimumHeight: 520
    minimumWidth: 360
    visible: true

    Shortcut {
        sequence: "ctrl+q"
        onActivated: mainWindow.close()
    }

    GridLayout {
        id: mainWinowGridLayout

        anchors.fill: parent

        columns: 2
        rows:3
        //        rowSpacing: 10
        //        columnSpacing: 10


        RowLayout {
            id: topRowLayout

            Layout.row: 0
            Layout.columnSpan: 2
//            Layout.margins: 20
            Layout.topMargin: 50
            Layout.alignment: Qt.AlignTop
            //        Layout.margins: 10

            //        anchors.top: parent.top
            //        anchors.left: parent.left
            //        anchors.right: parent.right
            //        anchors.margins: 30

            Button{
                id: exitButton

                Layout.fillWidth: true
                Layout.margins: 20

                height: 150

                property color exitButtonBackgroundColor: "lightgreen"
                onExitButtonBackgroundColorChanged:
                { exitButtonBackground.color = exitButtonBackgroundColor}

                text: qsTr("exit ")
                font.pointSize: 20

                contentItem: Text{
                    id: exitButtonText

                    text: exitButton.text
                    font: exitButton.font
                    color: "white"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }

                background: Rectangle{
                    id: exitButtonBackground

                    width: exitButton.width
                    height: exitButton.height
                    color: "lightgreen"
                    border.color: "white"
                    border.width: 0
                    radius: 100
                }

                MouseArea{
                    id: exitButtonMouseArea
                    anchors.fill: parent
                    hoverEnabled: true

                    // if on button clicked first time this button will be selected
                    // and I will draw it the first color (limegreen) else I will draw
                    // it the second color (lightgreen)
                    onPressed: {
                        mainWindow.close();
                    }
                    onReleased: {
                        exitButton.backButtonBackgroundColor = "lightgreen"
                    }
                }
            }

            Label{
                Layout.margins: 20

                text:  qsTr("Your physical indicators.")
                font.pointSize: 20

                color: "limegreen"

            }


        }

        ColumnLayout{
            id: columnLayout

            Layout.row: 1
            Layout.columnSpan: 2
            Layout.alignment: Qt.AlignRight

            Label{
                id: helloText
                Layout.alignment: Qt.AlignHCenter

                text:  qsTr("Hello!")
                font.pointSize: 50
                font.bold: true

            }

            Label{
                Layout.bottomMargin: 50;
                Layout.topMargin: 50;
                Layout.alignment: Qt.AlignHCenter;
                Layout.fillWidth: true;
                horizontalAlignment: Qt.AlignHCenter;

                wrapMode: Label.Wrap;
                elide: Text.ElideLeft;
                text:  qsTr("Let's start with physical indicators.");
                font.pointSize: 20;

//                clip: true;

            }

            GridLayout{
                id: menWomenButtonsGridLayout

                columns: 2
                rows: 1

                Button {
                    id: menButton

                    Layout.row: 1
                    Layout.column: 0
                    Layout.fillWidth: true
                    Layout.margins: 20

                    height: 150

                    property color menButtonBackgroundColor: "lightgreen"
                    onMenButtonBackgroundColorChanged:
                    { menButtonBackground.color = menButtonBackgroundColor}

                    text: qsTr("men")
                    font.pointSize: 20

                    contentItem: Text{
                        id: menButtonText

                        text: menButton.text
                        font: menButton.font
                        color: "white"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        elide: Text.ElideRight
                    }

                    background: Rectangle{
                        id: menButtonBackground

                        implicitWidth: menButton.width
                        implicitHeight: menButton.height
                        color: "lightgreen"
                        border.color: "white"
                        border.width: 0
                        radius: 100
                    }

                    MouseArea{
                        id: menButtonMouseArea
                        anchors.fill: parent
                        hoverEnabled: true

                        // if on button clicked first time this button will be selected
                        // and I will draw it the first color (limegreen) else I will draw
                        // it the second color (lightgreen)
                        onClicked: {
                            womenButton.womenButtonBackgroundColor = "lightgreen"
                            menButton.menButtonBackgroundColor = "limegreen"
                            columnLayout.anchors.top = topRowLayout.bottom
                            helloText.visible = false

                            weightBox.visible = true;
                            heightBox.visible = true;
                            ageBox.visible = true;

                            AppSettings.gender = 0;
                        }
                    }
                }


                Button {
                    id: womenButton

                    Layout.row: 1
                    Layout.column: 1
                    Layout.margins: 20

                    width: parent.width / 2.0 - 40
                    height: 150

                    property color womenButtonBackgroundColor: "lightgreen"
                    onWomenButtonBackgroundColorChanged:{
                        womenButtonBackground.color = womenButtonBackgroundColor}

                    text: qsTr("women")
                    font.pointSize: 20

                    contentItem: Text{
                        id: womenButtonText

                        text: womenButton.text
                        font: womenButton.font
                        color: "white"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        elide: Text.ElideRight
                    }

                    background: Rectangle{
                        id: womenButtonBackground

                        implicitWidth: womenButton.width
                        implicitHeight: womenButton.height
                        color: "lightgreen"
                        border.color: "white"
                        border.width: 0
                        radius: 100
                    }

                    MouseArea{
                        id: womenButtonMouseArea
                        anchors.fill: parent
                        hoverEnabled: true

                        // if on button clicked first time this button will be selected
                        // and I will draw it the first color (limegreen) else I will draw
                        // it the second color (lightgreen)
                        onClicked: {
                            womenButton.womenButtonBackgroundColor = "limegreen"
                            menButton.menButtonBackgroundColor = "lightgreen"
                            columnLayout.anchors.top = topRowLayout.bottom
                            helloText.visible = false

                            weightBox.visible = true;
                            heightBox.visible = true;
                            ageBox.visible = true;

                            AppSettings.gender = 1;
                        }
                    }
                }

            }

            FloatingTextField {
                id: ageBox

                Layout.columnSpan: 2
                Layout.fillWidth: true
                Layout.margins: 50

                property bool lastInputState: true

                inputMethodHints: Qt.ImhDigitsOnly
                validator: IntValidator{
                    bottom: 14;
                    top: 100;
                }

                selectByMouse: true
                enabled: root.enabledControls

                placeholderText: qsTr("Your age:")
                text: AppSettings.age;
                font.pointSize: 20;

                property bool thereIsAgeBottomRangeErrorLabel: false;
                property var ageBottomRangeErrorLabel;

                readonly property string ageBottomRangeErrorLabelText: 'import QtQuick 2.15;
import QtQuick.Controls 2.15;

Label{

anchors.top: ageBox.bottom;
anchors.left: ageBox.left;
anchors.right: ageBox.right;

wrapMode: Text.WordWrap;
text: qsTr("You must be at least 14 years old to use this application");
elide: Text.ElideRight;
font.pointSize: 14;
color: "red";
}'
                onAcceptableInputChanged: {
                    if(!acceptableInput && ageBox.text != ""){
                        if(!thereIsAgeBottomRangeErrorLabel){
                        ageBox.color = "lightgray";
                        ageBottomRangeErrorLabel = Qt.createQmlObject(ageBottomRangeErrorLabelText, columnLayout, "acceptableDynamicSnippet");
                            thereIsAgeBottomRangeErrorLabel = true;
                        }
                    }else if(acceptableInput){
                        if(thereIsAgeBottomRangeErrorLabel){
                            ageBottomRangeErrorLabel.destroy();
                            thereIsAgeBottomRangeErrorLabel = false;
                            ageBox.color = "black";
                        }
                        AppSettings.age = parseInt(ageBox.text, 10);
                        focus = false;
                    }
                }

                onActiveFocusChanged: {
                    if(!activeFocus && !acceptableInput && ageBox.text != ""){
                        if(!thereIsAgeBottomRangeErrorLabel){
                        ageBox.color = "lightgray";
                        ageBottomRangeErrorLabel = Qt.createQmlObject(ageBottomRangeErrorLabelText, columnLayout, "acceptableDynamicSnippet");
                            thereIsAgeBottomRangeErrorLabel = true;
                        }
                    }
                }
                visible: false;
            }

            FloatingTextField {
                id: heightBox

                Layout.columnSpan: 2
                Layout.fillWidth: true
                Layout.margins: 50

                inputMethodHints: Qt.ImhDigitsOnly
                validator: IntValidator {
                    bottom: 50
                    top: 300
                }

                selectByMouse: true
                enabled: root.enabledControls

                placeholderText: qsTr("Your height:")
                text: AppSettings.height;
                font.pointSize: 20;

                property bool thereIsHeightBottomRangeErrorLabel: false;
                property var heightBottomRangeErrorLabel;

                readonly property string heightBottomRangeErrorLabelText: 'import QtQuick 2.15;
import QtQuick.Controls 2.15;

Label{

anchors.top: heightBox.bottom;
anchors.left: heightBox.left;
anchors.right: heightBox.right;

wrapMode: Text.WordWrap;
text: qsTr("Your height must be more than 50 cm");
elide: Text.ElideRight;
font.pointSize: 14;
color: "red";
}'
                onAcceptableInputChanged: {
                    if(!acceptableInput && heightBox.text != ""){
                        if(!thereIsHeightBottomRangeErrorLabel){
                        heightBox.color = "lightgray";
                        heightBottomRangeErrorLabel = Qt.createQmlObject(heightBottomRangeErrorLabelText, columnLayout, "acceptableDynamicSnippet");
                            thereIsHeightBottomRangeErrorLabel = true;
                        }
                    }else if(acceptableInput){
                        if(thereIsHeightBottomRangeErrorLabel){
                            heightBottomRangeErrorLabel.destroy();
                            thereIsHeightBottomRangeErrorLabel = false;
                            heightBox.color = "black";
                        }
                        AppSettings.height = parseInt(heightBox.text);
                        focus = false;
                    }
                }

                onActiveFocusChanged: {
                    if(!activeFocus && !acceptableInput && heightBox.text != ""){
                        if(!thereIsHeightBottomRangeErrorLabel){
                        heightBox.color = "lightgray";
                        heightBottomRangeErrorLabel = Qt.createQmlObject(heightBottomRangeErrorLabelText, columnLayout, "acceptableDynamicSnippet");
                            thereIsHeightBottomRangeErrorLabel = true;
                        }
                    }
                }

                visible: false;
            }

            FloatingTextField {
                id: weightBox

                Layout.columnSpan: 2
                Layout.fillWidth: true
                Layout.margins: 50

                inputMethodHints: Qt.ImhDigitsOnly
                validator: IntValidator {
                    bottom: 30
                    top: 300
                }

                selectByMouse: true
                enabled: root.enabledControls

                placeholderText: qsTr("Your growth:")
                text: AppSettings.weight;
                font.pointSize: 20;

                property bool thereIsWeightBottomRangeErrorLabel: false;
                property var weightBottomRangeErrorLabel;

                readonly property string weightBottomRangeErrorLabelText: 'import QtQuick 2.15;
import QtQuick.Controls 2.15;

Label{

anchors.top: weightBox.bottom;
anchors.left: weightBox.left;
anchors.right: weightBox.right;

wrapMode: Text.WordWrap;
text: qsTr("Your weight must be over 30 kg");
elide: Text.ElideRight;
font.pointSize: 14;
color: "red";
}'
                onAcceptableInputChanged: {
                    if(!acceptableInput && weightBox.text != ""){
                        if(!thereIsWeightBottomRangeErrorLabel){
                        weightBox.color = "lightgray";
                        weightBottomRangeErrorLabel = Qt.createQmlObject(weightBottomRangeErrorLabelText, columnLayout, "acceptableDynamicSnippet");
                            thereIsWeightBottomRangeErrorLabel = true;
                        }
                    }else if(acceptableInput){
                        if(thereIsWeightBottomRangeErrorLabel){
                            weightBottomRangeErrorLabel.destroy();
                            thereIsWeightBottomRangeErrorLabel = false;
                            weightBox.color = "black";
                        }
                        AppSettings.weight = parseInt(weightBox.text);
                        focus = false;
                    }
                }

                onActiveFocusChanged: {
                    if(!activeFocus && !acceptableInput && weightBox.text != ""){
                        if(!thereIsWeightBottomRangeErrorLabel){
                        weightBox.color = "lightgray";
                        weightBottomRangeErrorLabel = Qt.createQmlObject(weightBottomRangeErrorLabelText, columnLayout, "acceptableDynamicSnippet");
                            thereIsWeightBottomRangeErrorLabel = true;
                        }
                    }
                }

                visible: false;
            }
        }
    }
}



/*##^##
Designer {
    D{i:0;autoSize:true;formeditorZoom:0.9;height:480;width:640}
}
##^##*/
