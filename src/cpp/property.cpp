#include "property.h"

#include <QCoreApplication>

QSharedPointer<GlobalSettings> GlobalSettings::m_instance;

QSharedPointer<GlobalSettings> GlobalSettings::instance()
{
    if (!m_instance)
        m_instance.reset(
            new GlobalSettings(QSettings::IniFormat,
                               QSettings::UserScope,
                               QCoreApplication::organizationName(),
                               QCoreApplication::applicationName()));

    return m_instance;
}

GlobalSettings::GlobalSettings(QSettings::Format format,
                               QSettings::Scope  scope,
                               const QString&    organization,
                               const QString&    application)
    : QSettings(format, scope, organization, application)
{
}
