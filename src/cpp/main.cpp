#include <QAudioInput>
#include <QFontDatabase>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlEngine>

#include "appsettings.h"
#include "models.h"
#include "singleton.h"

//#include "fontawesome.h"

int main(int argc, char* argv[])
{
    QGuiApplication::setApplicationName("Sport Nutrition");
    QGuiApplication::setOrganizationName(
        "Republican Scientific and Practical Sports Center");

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextProperty("AppSettings",
                                             SINGLETON(AppSettings));

    QFontDatabase fontDatabase;
    if (fontDatabase.addApplicationFont(
            "qrc:/fonts/Font Awesome 5 Duotone-Solid-900.otf") == -1)
        qWarning() << "Failed to load fontello.ttf";

    //    engine.addImportPath("qrc:///");
    engine.load(QUrl("qrc:/qml/main.qml"));

    return app.exec();
}
